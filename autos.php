<?php require_once "models/Auto.php";



$con = new Auto;
$con->makeConnection();
$operations['operation'] = '';
$error = ['mileage' => '', 'year' => '', 'make' => ''];

if (isset($_GET['name']) === false){
    die("Falta el parámetro"); 
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['logout']))
{
    header("Location: login.php") and die();
}


if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['add'])){

    if (is_numeric($_POST['mileage']) && is_numeric($_POST['year']) && isset($_POST['make']))
        {
            $auto = new Auto(htmlentities($_POST['make']), htmlentities($_POST['year']), htmlentities($_POST['mileage']));
            $con->addAuto($auto);
          
/*              $stmt = $pdo->prepare('INSERT INTO autos (make, year, mileage) VALUES ( :mk, :yr, :mi)');            
                $stmt->execute(array(':mk' => $_POST['make'], ':yr' => $_POST['year'], ':mi' => $_POST['mileage']));*/
            $operations['operation'] = "Record created"; 
          


        }else {
            if(!is_numeric($_POST['mileage']))
            {
               $error["mileage"] = "Mileage can only contain numbers";

            }

            if(!is_numeric($_POST['year'])){
                $error["year"] = "Year can only contain numbers";
            }

            $error["make"] = "Make field is required";
            $operations['operation'] = "";
        }

}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete'])){

    if (isset($_POST['name_del'])) {
        $con->delAuto($_POST['name_del']);
/*     $sql = "DELETE FROM autos WHERE auto_id = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(':id' => $_POST['name_del'])); */
    $operations['operation'] = "Record deleted";
    }else{
        $operations['operation'] = "";
    }

}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['logout']))
{
    header("Location: login.php") and die();
}


$autos = $con->getAutos(); 




 require_once "views/autos.view.php"; ?>