<?php require_once "lib/Database.php";?>

<?php

class Auto{

private $db;

private $make;

private $year;

private $mileage;

public function __construct($make = '', $year = 0, $mileage = 0){

 $this->make = $make;

 $this->year = $year;

 $this->mileage = $mileage;

}


public function getMake(){

    return $this->make;
}

public function getYear(){

    return $this->year;
}

public function getMileage(){

    return $this->mileage;
}


public function setMake($make){

   $this->make = $make;
}

public function setYear($year){

   $this->year = $year;
}

public function setMileage($mileage){

    $this->mileage = $mileage;
}

public function makeConnection(){

 $this->db = new Database();

}

public function getAutos(){

 $this->db->query("SELECT * FROM autos");

 $results = $this->db->resultSet('Auto');

 return $results;
}

public function addAuto($auto){

 $this->db->query('insert into autos (make,year,mileage) Values (:mk,:yr,:mi)');

 $this->db->bind(':mk', $auto->getMake());

 $this->db->bind(':yr', $auto->getYear());

 $this->db->bind(':mi', $auto->getMileage());

 $this->db->execute();

}

public function delAuto($auto){

 $this->db->query('delete from autos where auto_id = :id');

 $this->db->bind(':id', $auto);

 $this->db->execute();

}

public function rowCount(){

return  count($this->getAutos());
 
}

}
?>


