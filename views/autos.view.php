<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daniel Sanchez Perez</title>
</head>
<body>
    <div style=" <?= str_contains($operations['operation'], 'created') ? 'color:green;' :'color:red;' ?> ">
        <?php echo $operations['operation'] ?>
    </div>
    <form method="POST">
        <label for="make">Make</label>
        <input type="text" name="make" id="make">
        <label for="mileage">Kilometers</label>
        <input type="number" name="mileage" id="mileage">
        <label for="year">Year</label>
        <input type="number" name="year" id="year">
        <input type="submit" name="add" value="add">
    </form>
    <form method="post"><input type="submit" value="logout"></form>

    <table>

    <?php  foreach ($autos as $row): ?>
        
               <tr>
                   <td><?= $row->getMake(); ?></td>
                   <td><?= $row->getMileage(); ?></td>
                   <td><?= $row->getYear(); ?></td>
                   <td><form method="POST"><input type="hidden" name="name_del" value="<?php echo $row->auto_id; ?>"><input type="submit" name="delete" value="delete"></form></td>
               </tr>

    <?php endforeach; ?>
    </table>  
    <div>
        Total rows: <?= $con->rowCount(); ?>
    </div>
</body>
</html>